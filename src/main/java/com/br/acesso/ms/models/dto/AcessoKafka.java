package com.br.acesso.ms.models.dto;

import java.time.LocalDate;

public class AcessoKafka {
    private long porta_id;

    private long cliente_id;

    private boolean acessoValido;

    private LocalDate dataAtualizacao;

    public AcessoKafka() {
    }

    public long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(long porta_id) {
        this.porta_id = porta_id;
    }

    public long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(long cliente_id) {
        this.cliente_id = cliente_id;
    }

    public boolean isAcessoValido() {
        return acessoValido;
    }

    public void setAcessoValido(boolean acessoValido) {
        this.acessoValido = acessoValido;
    }

    public LocalDate getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(LocalDate dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }
}
