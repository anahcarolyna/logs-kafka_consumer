package com.br.acesso.ms.consumer;

import com.br.acesso.ms.models.dto.AcessoKafka;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-ana-carolina-1", groupId = "Carol-Costa")
    public void receber(@Payload AcessoKafka acessoKafka) throws CsvRequiredFieldEmptyException,
            IOException, CsvDataTypeMismatchException {

        generateCsvFile(acessoKafka);

        System.out.println("Recebi um acesso referentes as informações do cliente:" + acessoKafka.getCliente_id()
                + " , da porta " + acessoKafka.getPorta_id()
                + " , Autorizado: " + acessoKafka.isAcessoValido()
                + ", Data Atualizacao: " +  acessoKafka.getDataAtualizacao());
    }

    private static void generateCsvFile(AcessoKafka acessoKafka) throws IOException,
            CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

           Writer writer = new FileWriter("log.csv", true);
           StatefulBeanToCsv<AcessoKafka> beanToCsv =  new StatefulBeanToCsvBuilder(writer).build();

           beanToCsv.write(acessoKafka);
           
           writer.flush();
           writer.close();
    }
}
